/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 24 Dec 2017 
 */

/**
 * @file src/emulator.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <SDL2/SDL.h>

#include <emulatorconfig.h>
#include <chip8.h>
#include <render.h>
#include <emulator.h>

/*
 * Chip8 keyboard layout
 *
 *      1   2   3   C
 *      4   5   6   D
 *      7   8   9   E
 *      A   0   B   F
 */
const unsigned int keys[0x10] = {
    SDL_SCANCODE_X,     /* 0 */
    SDL_SCANCODE_1,     /* 1 */
    SDL_SCANCODE_2,     /* 2 */
    SDL_SCANCODE_3,     /* 3 */
    SDL_SCANCODE_A,     /* 4 */
    SDL_SCANCODE_Z,     /* 5 */
    SDL_SCANCODE_E,     /* 6 */
    SDL_SCANCODE_Q,     /* 7 */
    SDL_SCANCODE_S,     /* 8 */
    SDL_SCANCODE_D,     /* 9 */
    SDL_SCANCODE_W,     /* A */
    SDL_SCANCODE_C,     /* B */
    SDL_SCANCODE_4,     /* C */
    SDL_SCANCODE_R,     /* D */
    SDL_SCANCODE_F,     /* E */
    SDL_SCANCODE_V      /* F */
};
char *BASEPATH = NULL;


void emulator_reset(emulator_t *emulator){
    chip8_reset(&emulator->chip);
    emulator->timerDecrease = 0;
    emulator->loadedROM = NULL;
}

void emulator_init(emulator_t *emulator){
    srand(time(NULL));

    /* Config */
    BASEPATH = SDL_GetBasePath();
    if(!BASEPATH){
        BASEPATH = SDL_strdup("./");
    }
    config_load(BASEPATH);

    /* Init rendering */
    render_init();
    render_init_window(&emulator->graphics);
    emulator_reset(emulator);
}

int emulator_load_rom(emulator_t *emulator, char *rom){
    int loaded;
    char *path = malloc((strlen(ROMPATH) + strlen(rom) + 1) * sizeof(char));
    sprintf(path, "%s%s", ROMPATH, rom);
    loaded = rom_load(&emulator->chip.mem, path);
    free(path);
    if(loaded > 0){
        emulator->loadedROM = rom;
        return loaded;
    }
    return -1;
}

void emulator_run(emulator_t *emulator){
    SDL_Event event;
    chip8_t *chip = &emulator->chip;
    int timer = SDL_GetTicks();

    while(chip->cpu.state != CPU_STOP){
        if(SDL_PollEvent(&event)){
            switch(event.type){
                case SDL_QUIT:
                    chip->cpu.state = CPU_STOP;
                    break;
                case SDL_KEYDOWN:
                    if(event.key.keysym.scancode == SDL_SCANCODE_ESCAPE){
                        chip->cpu.state = CPU_STOP;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    render_handle_event(emulator, &event);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    render_handle_event(emulator, &event);
                    break;
                default: break;
            }
        }

        emulator_update(emulator);
        cpu_execute_cycle(chip, &emulator->timerDecrease);
        render_draw(chip->graphics, &emulator->graphics);

        SDL_Delay(1);
        if(SDL_GetTicks() - timer >= CYCLE_TIME_MS){
            emulator->timerDecrease = 1;
            timer = SDL_GetTicks();
        }
    }
}

void emulator_update(emulator_t *emulator){
    unsigned int i;
    const Uint8 *kstate = SDL_GetKeyboardState(NULL);
    for(i = 0; i < 0x10; i++){
        emulator->chip.input[i] = kstate[keys[i]];
    }
}

void emulator_quit(emulator_t *emulator){
    free(BASEPATH);
    free(ROMPATH);
    render_destroy(&emulator->graphics);
}


/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 02 Jan 2018 
 */

/**
 * @file src/rom_selection_panel.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <SDL2/SDL.h>
#include <linkedlist.h>

#include <render.h>
#include <emulatorconfig.h>
#include <chip8config.h>
#include <rom_selection_panel.h>

static linkedlist_t _get_roms_name();


rompanel_t* init_rom_selection_panel(){
    rompanel_t *rompanel = malloc(sizeof(struct ROMSelectionPanel));

    rompanel->panel.viewport.x = 0;
    rompanel->panel.viewport.y = SQUARE_SIZE * DISPLAY_HEIGHT;
    rompanel->panel.viewport.w = SQUARE_SIZE * DISPLAY_WIDTH;
    rompanel->panel.viewport.h = 100;
    panel_init_relative(&rompanel->panel);
    panel_background_color(&rompanel->panel, 33, 33, 33, SDL_ALPHA_OPAQUE);
    /* TODO: change 200 with const in emulatorconfig.h */
    rompanel->buttons = _get_roms_name();
    rompanel->needsUpdate = 1;

    rompanel->hovering.x = 0; rompanel->hovering.y = 0;
    rompanel->hovering.w = 0; rompanel->hovering.h = 0;

    rompanel->textcolor.r = (TEXT_COLOR >> 6*4) & 0xFF;
    rompanel->textcolor.g = (TEXT_COLOR >> 4*4) & 0xFF;
    rompanel->textcolor.b = (TEXT_COLOR >> 2*4) & 0xFF;
    rompanel->textcolor.a = TEXT_COLOR & 0xFF;

    return rompanel;
}

void rom_selection_panel_destroy(rompanel_t *panel){
    ll_destroy(panel->buttons);
}

void rom_selection_panel_draw(const graphics_t *graphics, rompanel_t *rompanel){
    SDL_Texture *text = NULL;
    int w, h;
    SDL_Rect dest;
    dest.x = 10; dest.y = 10;   /* TODO: Padding values => into config file */

    if(!rompanel->needsUpdate){
        return;
    }

    SDL_RenderSetViewport(graphics->renderer, &rompanel->panel.viewport);
    panel_fill_background(graphics->renderer, &rompanel->panel);

    while(ll_has_next(rompanel->buttons)){
        rombutton_t button = (rombutton_t)ll_next(rompanel->buttons);
        text = render_create_text(graphics, button->label, rompanel->textcolor, &w, &h);
        dest.w = w; dest.h = h;
        if(dest.x + w >= rompanel->panel.viewport.w){
            dest.y += h + 10;
            dest.x = 10;
        }
        SDL_RenderCopy(graphics->renderer, text, NULL, &dest);
        button->position = dest; 
        dest.x += w + 10;
        if(dest.x >= rompanel->panel.viewport.w){
            dest.x = 10;
            dest.y += h + 10;
        }
        SDL_DestroyTexture(text);
    }

    SDL_SetRenderDrawColor(
            graphics->renderer,
            (HOVERING_COLOR >> 6*4) & 0xFF,
            (HOVERING_COLOR >> 4*4) & 0xFF,
            (HOVERING_COLOR >> 2*4) & 0xFF,
            (HOVERING_COLOR & 0xFF));
    SDL_RenderFillRect(graphics->renderer, &rompanel->hovering);

    SDL_RenderPresent(graphics->renderer);
    rompanel->needsUpdate = 0;
}

rombutton_t rombutton_create(char *label, SDL_Rect position){
    rombutton_t button = malloc(sizeof(struct ROMButton));
    button->label = label;
    button->position = position;
    return button;
}

void rombutton_free(void *button){
    free(((rombutton_t)button)->label);
    free(button);
}

int rompanel_inside_button(SDL_Rect butpos, const SDL_Event *event){
    int x = event->motion.x;
    int y = event->motion.y - SQUARE_SIZE * DISPLAY_HEIGHT;

    return (x >= butpos.x && x <= butpos.x + butpos.w && 
            y >= butpos.y && y <= butpos.y + butpos.h);
}

void rompanel_handle_mousemotion(rompanel_t *rompanel, const SDL_Event *event){
    rompanel->needsUpdate = 1;
    /* Set hovering button location in order to color it */
    rompanel->hovering.x = 0; rompanel->hovering.y = 0; 
    rompanel->hovering.w = 0; rompanel->hovering.h = 0; 
    while(ll_has_next(rompanel->buttons)){
        rombutton_t button = ll_next(rompanel->buttons);
        if(rompanel_inside_button(button->position, event)){
            rompanel->hovering = button->position;
            /* Adding Padding */
            rompanel->hovering.x -= 5; rompanel->hovering.y -= 5;
            rompanel->hovering.w += 10; rompanel->hovering.h += 10;
            /* Reset the linked list current to the head */
            rompanel->buttons->current = rompanel->buttons->head;
            break;
        }
    }
}

char* rompanel_handle_buttondown(rompanel_t *rompanel, const SDL_Event *event){
    while(ll_has_next(rompanel->buttons)){
        rombutton_t button = ll_next(rompanel->buttons);
        if(rompanel_inside_button(button->position, event)){
            /* Reset the linked list current to the head */
            rompanel->buttons->current = rompanel->buttons->head;
            return button->label;
        }
    }
    return NULL;
}

static linkedlist_t _get_roms_name(){
    linkedlist_t buttons = ll_create();
    rombutton_t button = NULL;
    struct dirent *dp;
    struct stat stbuf;
    DIR *dfd;
    char *filepath = NULL;
    unsigned int length = 0;

    if(!(dfd = opendir(ROMPATH))){
        fprintf(stderr, "Couldn't open %s\n", ROMPATH);
        return buttons;
    }

    while((dp = readdir(dfd))){
        if(strlen(dp->d_name) > length){
            free(filepath);
            length = strlen(dp->d_name) + strlen(ROMPATH) + 1;
            filepath = malloc(length * sizeof(char));
        }
        sprintf(filepath, "%s%s", ROMPATH, dp->d_name);
        if(stat(filepath, &stbuf) < 0){
            continue;
        }
        /* Skip directories */
        /*if((stbuf.st_mode & S_IFMT) == S_IFDIR){*/
            /*continue;*/
        /*}*/
        /* temporary */
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0){
            continue;
        }

        button = malloc(sizeof(struct ROMButton));
        button->label = SDL_strdup(dp->d_name);
        ll_add(buttons, button);
    }

    closedir(dfd);
    free(filepath);
    return buttons;
}

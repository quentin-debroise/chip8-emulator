/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 *
 * @todo
 * @bug
 */

/**
 * @file src/panel.c
 *
 * DESCRIPTION
 */


#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include <panel.h>

void panel_fill_background(SDL_Renderer *renderer, const panel_t *panel){
    SDL_SetRenderDrawColor(
            renderer,
            panel->bgcolor.r,
            panel->bgcolor.g,
            panel->bgcolor.b,
            panel->bgcolor.a);
    SDL_RenderFillRect(renderer, &panel->relative);
}

void panel_init_relative(panel_t *panel){
    panel->relative = panel->viewport;
    panel->relative.x = 0;
    panel->relative.y = 0;
}

void panel_background_color(panel_t *panel,
        unsigned int r,
        unsigned int g,
        unsigned int b,
        unsigned int a){
    SDL_Color color;
    color.r = r;
    color.g = g;
    color.b = b;
    color.a = a;
    panel->bgcolor = color;
}

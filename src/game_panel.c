/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 *
 * @todo
 * @bug
 */

/**
 * @file src/game_panel.c
 *
 * DESCRIPTION
 */


#include <stdio.h>
#include <stdlib.h>

#include <chip8config.h>
#include <game_panel.h>

panel_t* init_game_panel(){
    panel_t *panel = malloc(sizeof(struct Panel));
    panel->viewport.x = 0;
    panel->viewport.y = 0;
    panel->viewport.w = SQUARE_SIZE * DISPLAY_WIDTH;
    panel->viewport.h = SQUARE_SIZE * DISPLAY_HEIGHT;
    panel_init_relative(panel);
    panel_background_color(panel, 53, 52, 42, SDL_ALPHA_OPAQUE);
    return panel;
}

void game_panel_draw(chip8_graphics_t gfx, const panel_t *panel, SDL_Renderer *renderer){
    unsigned int i, j;
    SDL_Rect rect;
    rect.w = SQUARE_SIZE;
    rect.h = SQUARE_SIZE;

    SDL_RenderSetViewport(renderer, &panel->viewport);
    panel_fill_background(renderer, panel);

    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    for(i = 0; i < DISPLAY_HEIGHT; i++){
        for(j = 0; j < DISPLAY_WIDTH; j++){
            if(gfx[i * DISPLAY_WIDTH + j]){
                rect.x = j * SQUARE_SIZE;
                rect.y = i * SQUARE_SIZE;
                SDL_RenderFillRect(renderer, &rect);
            }
        }
    }
}


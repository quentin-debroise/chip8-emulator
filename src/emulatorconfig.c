/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 02 Jan 2018 
 */

/**
 * @file src/emulatorconfig.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <emulatorconfig.h>

/*
 * Constants default values
 */
char *ROMPATH = "./";
int SQUARE_SIZE = 10;
char *ROM_FOLDER = "./";
char *FONT= "./";
long HOVERING_COLOR = 0x00000099;
long TEXT_COLOR = 0xFFFFFFFF;
int FONT_SIZE = 18;



void config_load(char *basepath){
    char *configpath = malloc((strlen(basepath) + strlen("../config.txt") + 1) * sizeof(char));
    FILE *f;
    char *buffer = malloc(256 * sizeof(char));
    char *key, *value;

    sprintf(configpath, "%s../config.txt", basepath);
    if(!(f = fopen(configpath, "r"))){
        fprintf(stderr, "Error loading config file (%s)\n", configpath);
        exit(1);
    }

    while(fgets(buffer, 256, f)){
        if(*buffer == '#') continue;    /* Igore comments */
        if(*buffer == '\n') continue;   /* Ignore blank lines */
        buffer[strlen(buffer) - 1] = '\0';
        key = strtok(buffer, "=");
        value = strtok(NULL, "=");

        if(strcmp(key, "SQUARE_SIZE") == 0){
            SQUARE_SIZE = atoi(value);
        }
        else if(strcmp(key, "ROM_FOLDER") == 0){
            ROM_FOLDER = malloc((strlen(value) + 1) * sizeof(char));
            strcpy(ROM_FOLDER, value);
        }
        else if(strcmp(key, "FONT") == 0){
            FONT = malloc((strlen(basepath) + strlen(value) + 1) * sizeof(char));
            sprintf(FONT, "%s%s", basepath, value);
        }
        else if(strcmp(key, "HOVERING_COLOR") == 0){
            HOVERING_COLOR = strtol(value, NULL, 16);
        }
        else if(strcmp(key, "TEXT_COLOR") == 0){
            TEXT_COLOR = strtol(value, NULL, 16);
        }
        else if(strcmp(key, "FONT_SIZE") == 0){
            FONT_SIZE = atoi(value);
            printf("size : %d\n", FONT_SIZE);
        }
    }

    /* Sets path to ROMs */
    ROMPATH = malloc((strlen(BASEPATH) + strlen(ROM_FOLDER) + 1) * sizeof(char));
    sprintf(ROMPATH, "%s%s", BASEPATH, ROM_FOLDER);
    printf("rompath: %s\n", ROMPATH);

    printf("** Loaded config file: %s\n", configpath);
    free(configpath);
    free(buffer);
}



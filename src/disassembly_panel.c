/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 *
 * @todo
 * @bug
 */

/**
 * @file src/disassembly_panel.c
 *
 * DESCRIPTION
 */


#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL_ttf.h>

#include <render.h>
#include <chip8config.h>
#include <emulatorconfig.h>
#include <disassembly_panel.h>

panel_t* init_disassembly_panel(){
    panel_t *panel = malloc(sizeof(struct Panel));
    panel->viewport.x = SQUARE_SIZE * DISPLAY_WIDTH;
    panel->viewport.y = 0;
    panel->viewport.w = 200;
    panel->viewport.h = SQUARE_SIZE * DISPLAY_HEIGHT;
    panel_init_relative(panel);
    panel_background_color(panel, 33, 33, 33, SDL_ALPHA_OPAQUE);
    return panel;
}

void disassembly_panel_draw(const graphics_t *graphics, const panel_t *panel){
    int w, h;
    SDL_Color color = {255, 255, 255, 255};
    SDL_Texture *texture = render_create_text(graphics, "Welcome Aliens!", color, &w,  &h);
    SDL_Rect dest;
    dest.x = 0; dest.y = 0; dest.w = w; dest.h = h;

    SDL_RenderSetViewport(graphics->renderer, &panel->viewport);
    panel_fill_background(graphics->renderer, panel);
    SDL_RenderCopy(graphics->renderer, texture, NULL, &dest);
    SDL_SetRenderDrawColor(graphics->renderer, 255, 0, 0, 64);
    dest.w = panel->relative.w;
    SDL_RenderFillRect(graphics->renderer, &dest);
    SDL_DestroyTexture(texture);
}

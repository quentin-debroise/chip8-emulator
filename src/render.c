/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 20 Dec 2017 
 */

/**
 * @file src/render.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <emulator.h>
#include <chip8_graphics.h>
#include <chip8_input.h>
#include <game_panel.h>
#include <disassembly_panel.h>
#include <rom_selection_panel.h>
#include <render.h>

static void _get_window_size(panel_t* const *panels, unsigned int nbPanels, int *w, int *h);
static int _in_selection_panel(const SDL_Event *evnet);


void render_init(){
    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    if(TTF_Init() < 0){
        fprintf(stderr, "Couldn't initialize SDL_ttf: %s\n", TTF_GetError());
        exit(1);
    }
}

void render_init_window(struct Graphics *graphics){
    int w, h;
    graphics->panels[0] = init_game_panel();
    graphics->panels[1] = init_disassembly_panel(graphics->panels[1]);
    graphics->rompanel = init_rom_selection_panel();
    graphics->panels[2] = &graphics->rompanel->panel;
    _get_window_size(graphics->panels, 3, &w, &h);

    graphics->window = SDL_CreateWindow(
            "CHIP-8 Emulator",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            w, h,
            0);
    if(!graphics->window){
        fprintf(stderr, "Couldn't create the SDL window! Error: %s\n", SDL_GetError());
        exit(1);
    }

    graphics->renderer = SDL_CreateRenderer(graphics->window, -1, SDL_RENDERER_ACCELERATED);
    if(!graphics->renderer){
        fprintf(stderr, "Couldn't initialize SDL renderer! Error: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_SetRenderDrawColor(graphics->renderer, 53, 52, 42, 255);
    SDL_SetRenderDrawBlendMode(graphics->renderer, SDL_BLENDMODE_BLEND);

    graphics->font = TTF_OpenFont(FONT, FONT_SIZE); /* TODO: config file => font path */
    if(!graphics->font){
        fprintf(stderr, "Couldn't open font file! %s\n", TTF_GetError());
        exit(1);
    }
}

void render_destroy(struct Graphics *graphics){
    free(graphics->panels[0]);
    free(graphics->panels[1]);
    free(graphics->panels[2]);

    TTF_CloseFont(graphics->font);
    SDL_DestroyWindow(graphics->window);
    TTF_Quit();
    SDL_Quit();
}

void render_draw(chip8_graphics_t gfx, const struct Graphics *graphics){
    SDL_SetRenderDrawColor(graphics->renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);

    game_panel_draw(gfx, graphics->panels[0], graphics->renderer);
    disassembly_panel_draw(graphics, graphics->panels[1]);
    rom_selection_panel_draw(graphics, graphics->rompanel);

    SDL_RenderPresent(graphics->renderer);
}

SDL_Texture* render_create_text(const struct Graphics *graphics, const char *text, SDL_Color color, int *w, int *h){
    SDL_Texture *texture;
    SDL_Surface *surface = TTF_RenderText_Solid(graphics->font, text, color);
    if(!surface){
        fprintf(stderr, "Couldn't render text to surface! %s\n", TTF_GetError());
        return NULL;
    }
    *w = surface->w;
    *h = surface->h;
    texture = SDL_CreateTextureFromSurface(graphics->renderer, surface);
    if(!texture){
        fprintf(stderr, "Couldn't create texture! %s\n", SDL_GetError());
        SDL_FreeSurface(surface);
        return NULL;
    }

    SDL_FreeSurface(surface);
    return texture;
}

void render_handle_event(emulator_t *emulator, const SDL_Event *event){
    char *rom;
    if(_in_selection_panel(event)){
        if(event->type == SDL_MOUSEMOTION){
            rompanel_handle_mousemotion(emulator->graphics.rompanel, event);
        }
        else if(event->type == SDL_MOUSEBUTTONDOWN){
            if((rom = rompanel_handle_buttondown(emulator->graphics.rompanel, event))){
                emulator_reset(emulator);
                emulator_load_rom(emulator, rom);
            }
        }
    }
}



static int _in_selection_panel(const SDL_Event *event){
    int x, y;
    x = event->motion.x;
    y = event->motion.y - SQUARE_SIZE * DISPLAY_HEIGHT;
    return (x <= SQUARE_SIZE * DISPLAY_WIDTH && y >= 0);
}

static void _get_window_size(panel_t* const *panels, unsigned int nbPanels, int *w, int *h){
    unsigned int i;

    *w = 0; *h = 0;
    for(i = 0; i < nbPanels; i++){
        if(panels[i]->viewport.x >= *w){
            *w += panels[i]->viewport.w;
        }
        if(panels[i]->viewport.y >= *h){
            *h += panels[i]->viewport.h;
        }
    }
}

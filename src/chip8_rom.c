/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 19 Dec 2017 
 */

/**
 * @file src/chip8_rom.c
 */


#include <stdio.h>
#include <stdlib.h>

#include <chip8_mem.h>
#include <chip8_rom.h>


unsigned int rom_load(struct Chip8Mem *mem, const char *filepath){
    unsigned int i;
    unsigned int nbbytes;
    unsigned char *codebuffer;

    nbbytes = rom_read(filepath, &codebuffer);
    for(i = 0; i < nbbytes; i++){
        mem->memory[PROGRAM_ADDR + i] = codebuffer[i];
    }

    if(nbbytes){
        printf("*** Loaded %s - %d bytes\n", filepath, nbbytes);
    }
    free(codebuffer);
    return nbbytes;
}

void rom_hexdump(const char *filepath){
    unsigned int i;
    unsigned int fsize;
    unsigned char *codebuffer;

    if(!(fsize = rom_read(filepath, &codebuffer))){
        return;
    }

    printf("Bytes: %d\n", fsize);
    for(i = 0; i < fsize; i++){
        printf("%.2x", codebuffer[i]);
        if((i + 1) % 16 == 0){
            printf("\n");
        }
        else if((i + 1) % 8 == 0){
            printf("  ");
        }
        else if((i + 1) % 2 == 0){
            printf(" ");
        }
    }
    printf("\n");

    free(codebuffer);
}

unsigned int rom_read(const char *filepath, unsigned char **buffer){
    unsigned int fsize;
    FILE *f = fopen(filepath, "r");

    if(!f){
        fprintf(stderr, "The file %s cannot be found!\n", filepath);
        *buffer = NULL;
        return 0;
    }

    fseek(f, 0, SEEK_END);
    fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    *buffer = malloc(fsize * sizeof(char));

    /* Read the ROM and store all the code in the buffer */
    fread(*buffer, sizeof(char), fsize, f);

    fclose(f);
    return fsize;
}

void rom_dissassemble(const unsigned char *codebuffer, unsigned int codeLength){
    unsigned int i;
    unsigned short int operation;

    printf("Bytes: %d\n", codeLength);
    printf("Instructions: %d\n", codeLength / 2);
    for(i = 0; i < codeLength; i += 2){
        printf("%.8x\t", PROGRAM_ADDR + i);
        operation = 0 | ((codebuffer[i] << 8) | codebuffer[i + 1]);
        printf("%.4x\t", operation);
        switch((operation & 0xF000) >> 12){
            case 0x0:
                switch(operation & 0x0FFF){
                    case 0x00e0: printf("CLS"); break;
                    case 0x00ee: printf("RET"); break;
                    default: printf("SYS 0x%.4x", operation & 0x0FFF); break;
                }
                break;
            case 0x1: printf("JUMP 0x%.4x", operation & 0x0FFF); break;
            case 0x2: printf("CALL 0x%.4x", operation & 0x0FFF); break;
            case 0x3: printf("SE V%d, $0x%.2x",
                              (operation & 0x0F00) >> 8,
                              operation & 0x00FF);
                      break;
            case 0x4: printf("SNE V%d, $0x%.2x", 
                              (operation & 0x0F00) >> 8, 
                              operation & 0x00FF);
                      break;
            case 0x5: printf("SE V%d, V%d", 
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 8);
                      break;
            case 0x6: printf("LD V%d, $0x%.2x", 
                              (operation & 0x0F00) >> 8,
                              operation & 0x00FF);
                      break;
            case 0x7: printf("ADD V%d, $0x%.2x", 
                              (operation & 0x0F00) >> 8,
                              operation & 0x00FF);
                      break;
            case 0x8: 
                switch(operation & 0x000F){
                    case 0x0:
                        printf("LD V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x1:
                        printf("OR V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x2:
                        printf("AND V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x3:
                        printf("XOR V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x4:
                        printf("ADD V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x5:
                        printf("SUB V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x6:
                        printf("SHR V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0x7:
                        printf("SUBN V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                    case 0xe:
                        printf("SHL V%d, V%d",
                              (operation & 0x0F00) >> 8,
                              (operation & 0x00F0) >> 4);
                        break;
                }
                break;
            case 0x9:
                printf("SNE V%d, V%d",
                        (operation & 0x0F00) >> 8,
                        (operation & 0x00F0) >> 4);
                break;
            case 0xa: printf("LD I, 0x%.4x", operation & 0x0FFF); break;
            case 0xb: printf("JP V0, 0x%.4x", operation & 0x0FFF); break;
            case 0xc: printf("RND V%d, $0x%.2x",
                            (operation & 0x0F00) >> 8,
                            operation & 0x00FF);
                      break;
            case 0xd: printf("DRW V%d, V%d, %d",
                            (operation & 0x0F00) >> 8,
                            (operation & 0x00F0) >> 4,
                            operation & 0x000F);
                      break;
            case 0xe:
                switch(operation & 0x00FF){
                    case 0x9e: printf("SKP V%d", (operation & 0x0F00) >> 8); break;
                    case 0xa1: printf("SKNP V%d", (operation & 0x0F00) >> 8); break;
                    default: printf("Unsupported operation");
                }
                break;
            case 0xf:
                switch(operation & 0x00FF){
                    case 0x07: printf("LD V%d, DT", (operation & 0x0F00) >> 8); break;
                    case 0x0a: printf("LD V%d, K", (operation & 0x0F00) >> 8); break;
                    case 0x15: printf("LD DT, V%d", (operation & 0x0F00) >> 8); break;
                    case 0x18: printf("LD ST, V%d", (operation & 0x0F00) >> 8); break;
                    case 0x1e: printf("ADD I, V%d", (operation & 0x0F00) >> 8); break;
                    case 0x29: printf("LD F, V%d", (operation & 0x0F00) >> 8); break;
                    case 0x33: printf("LD B, V%d", (operation & 0x0F00) >> 8); break;
                    case 0x55: printf("LD [I], V%d", (operation & 0x0F00) >> 8); break;
                    case 0x65: printf("LD V%d, [I]", (operation & 0x0F00) >> 8); break;
                }
                break;
            default:
                printf("unsupported operation");
        }
        printf("\n");
    }
}


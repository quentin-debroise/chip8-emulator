/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 18 Dec 2017 
 */

/**
 * @file src/chip8_graphics.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <chip8_graphics.h>

void display_clear(chip8_graphics_t graphics){
    memset(graphics, 0, DISPLAY_WIDTH * DISPLAY_HEIGHT * sizeof(unsigned char));
}

/**
 * Draw the displat to standard output with zeros and ones.
 *
 * @param graphics Chip8 graphics.
 */
void display(chip8_graphics_t graphics){
    unsigned int i, j;
    
    for(i = 0; i < DISPLAY_HEIGHT; i++){
        for(j = 0; j < DISPLAY_WIDTH; j++){
            printf("%d ", graphics[j + i * DISPLAY_WIDTH]);
        }
        printf("\n");
    }
}

/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 09 Dec 2017 
 */

/**
 * @file src/main.c
 *
 * CHIP-8 main program.
 */


#include <stdio.h>
#include <stdlib.h>

#include <chip8.h>
#include <render.h>
#include <emulator.h>

int main(int argc, char *argv[]){
    emulator_t emulator;

    if(argc != 2){
        fprintf(stderr, "You must provide a ROM\n");
        exit(1);
    }

    /*unsigned char *buffer = NULL;*/
    /*unsigned int length;*/
    /*length = rom_read(argv[1], &buffer);*/
    /*rom_dissassemble(buffer, length);*/

    emulator_init(&emulator);
    emulator.loadedROM = argv[1];
    if(emulator_load_rom(&emulator, argv[1]) > 0){
        emulator_run(&emulator);
    }
    emulator_quit(&emulator);

    return 0;
}



/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 09 Dec 2017 
 */

/**
 * @file src/chip8.c
 *
 * CHIP-8 emulator.
 */


#include <stdio.h>
#include <stdlib.h>

#include <chip8_mem.h>
#include <chip8_graphics.h>
#include <chip8.h>
#include <chip8_cpu.h>
#include <chip8_rom.h>
#include <sys/time.h>

#ifdef _WIN32
    #include <windows.h>
    void delay_ms(unsigned int milliseconds){
        Sleep(milliseconds);
    }
#else
    #include <unistd.h>
    void delay_ms(unsigned int milliseconds){
        usleep(1000 * milliseconds);
    }
#endif


void chip8_dump(chip8_t chip){
    unsigned int i;

    printf("\n\n===================================\n");
    printf("--- Registers ---\n");
    printf("PC = 0x%.4x\tI = 0x%.4x\n\n", chip.cpu.pc, chip.cpu.i);
    for(i = 0; i < NB_REGISTERS; i+=2){
        printf("V%d = 0x%.2x\tV%d = 0x%.2x\n",
                i, 
                chip.cpu.v[i],
                i + 1,
                chip.cpu.v[i + 1]);
    }

    printf("\n--- Stack ---\n");
    printf("SP = %d\n", chip.cpu.sp);
    for(i = STACK_SIZE; i > 0; i--){
        printf("%d\t0x%.4x\n", i - 1, chip.mem.stack[i - 1]);
    }

    printf("\n--- Timers ---\n");
    printf("DT = %d\tST = %d\n", chip.cpu.delay, chip.cpu.sound);
    printf("===================================\n\n");
}

void chip8_reset(chip8_t *chip){
    display_clear(chip->graphics);
    cpu_reset(&chip->cpu);
    chip8_mem_init(&chip->mem);
    input_reset(chip->input);
}

void chip8_execute_step_by_step(chip8_t *chip){
    char c;
    int timerDecrease = 1;

    while(1){
        printf("\n\n\n============================================\n");
        cpu_execute_cycle(chip, &timerDecrease);
        timerDecrease = 1;
        chip8_dump(*chip);
        printf("Next instruction: %.4x ....\n", chip->cpu.pc);
        scanf("%c", &c);
    }
}

void chip8_execute_n(chip8_t *chip, unsigned int n){
    unsigned int i;
    int timerDescrease = 1;
    for(i = 0; i < n; i++){
        cpu_execute_cycle(chip, &timerDescrease);
        timerDescrease = 1;
    }
    chip8_dump(*chip);
}

void chip8_execute(chip8_t *chip){
    struct timeval current;
    int timer;
    int currentms;
    int decreaseTimer = 0;

    gettimeofday(&current, NULL);
    timer = (current.tv_sec * 1000) + current.tv_usec / 1000;
    while(1){
        cpu_execute_cycle(chip, &decreaseTimer);

        delay_ms(1);
        gettimeofday(&current, NULL);
        currentms = (current.tv_sec * 1000) + current.tv_usec / 1000;

        if(currentms - timer >= CYCLE_TIME_MS){
            decreaseTimer = 1;
            gettimeofday(&current, NULL);
            timer = (current.tv_sec * 1000) + current.tv_usec / 1000;
        }
    }
}


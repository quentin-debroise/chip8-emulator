/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 20 Dec 2017 
 */

/**
 * @file src/chip8_input.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <chip8_input.h>


void input_reset(chip8_input_t input){
    memset(input, 0, NUMBER_OF_KEYS * sizeof(unsigned char));
}

int input_is_key_pressed(chip8_input_t input, unsigned char key){
    return (input[key] == 1);
}


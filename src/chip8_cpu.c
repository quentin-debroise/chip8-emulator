/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 10 Dec 2017 
 */

/**
 * @file src/chip8_cpu.c
 *
 * CHIP-8 CPU functions for executing each instruction.
 */


#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include <chip8.h>

static const ptr_execute_nibble_t execute[NB_NIBBLES] = {
    &execute_nibble_0,
    &execute_nibble_1,
    &execute_nibble_2,
    &execute_nibble_3,
    &execute_nibble_4,
    &execute_nibble_5,
    &execute_nibble_6,
    &execute_nibble_7,
    &execute_nibble_8,
    &execute_nibble_9,
    &execute_nibble_a,
    &execute_nibble_b,
    &execute_nibble_c,
    &execute_nibble_d,
    &execute_nibble_e,
    &execute_nibble_f,
};


void cpu_reset(struct Chip8CPU *cpu){
    unsigned int i;

    for(i = 0; i < NB_REGISTERS; i++){
        cpu->v[i] = 0;
    }
    cpu->pc = PROGRAM_ADDR;
    cpu->sp = 0;
    cpu->i = 0;
    cpu->delay = 0;
    cpu->sound = 0;

    /*cpu->state = CPU_DEBUG;*/
    cpu->state = CPU_RUNNING;
}

void cpu_execute_cycle(struct Chip8 *chip, int *timerDecrease){
    unsigned short int operation;
    unsigned char nibble;

    /* Fetch */
    operation = 0 |
        ((chip->mem.memory[chip->cpu.pc] << 8) |
        chip->mem.memory[chip->cpu.pc + 1]);
    chip->cpu.pc += 2;

    /* Decode & execute */
    if(chip->cpu.state == CPU_DEBUG){
        printf("%.8x\t%s", chip->cpu.pc - 2, chip->cpu.debug); 
    } 
    nibble = (operation & 0xF000) >> 12;
    execute[(unsigned int)nibble](chip, operation);

    if(*timerDecrease){
        if(chip->cpu.delay) chip->cpu.delay--;
        if(chip->cpu.sound) chip->cpu.sound--;
        *timerDecrease = 0;
    }
}


void execute_nibble_0(struct Chip8 *chip, unsigned short int operation){
    unsigned char low = 0 | (operation & 0x00FF);

    switch(low){
        case 0xe0:
            sprintf(chip->cpu.debug, "(0) - Clearing display\n");
            display_clear(chip->graphics);
            break;
        case 0xee:
            sprintf(chip->cpu.debug, "(0) - Returning from subroutine\n");
            (chip->cpu.sp)--;
            chip->cpu.pc = chip->mem.stack[chip->cpu.sp];
            break;
        default:
            sprintf(chip->cpu.debug, "Error: (0) - Unknown opcode: %.4x", operation);
            break;
    }
}

void execute_nibble_1(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(1) Jumping to address 0x%.4x\n", operation & 0x0FFF);
    chip->cpu.pc = operation & 0x0FFF;
}

void execute_nibble_2(struct Chip8 *chip, unsigned short int operation){ sprintf(chip->cpu.debug, "(2) - Calling subroutine at address 0x%.4x\n", operation & 0x0FFF);
    chip->mem.stack[chip->cpu.sp] = chip->cpu.pc;
    (chip->cpu.sp)++;
    chip->cpu.pc = operation & 0x0FFF;
}

void execute_nibble_3(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(3) Skip if V%d = $0x%.2x\n", (operation & 0x0F00) >> 8, operation & 0x00FF);
    if(chip->cpu.v[(operation & 0x0F00) >> 8] == (operation & 0x00FF)){
        chip->cpu.pc += 2;
    }
}

void execute_nibble_4(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(4) Skip if V%d != $0x%.2x\n", (operation & 0x0F00) >> 8, operation & 0x00FF);
    if(chip->cpu.v[(operation & 0x0F00) >> 8] != (operation & 0x00FF)){
        chip->cpu.pc += 2;
    }
}

void execute_nibble_5(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(5) Skip if V%d = V%d\n", (operation & 0x0F00) >> 8, (operation & 0x00F0) >> 4);
    if(chip->cpu.v[(operation & 0x0F00) >> 8] == chip->cpu.v[(operation & 0x00F0) >> 4]){
        chip->cpu.pc += 2;
    }
}

void execute_nibble_6(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(6) - Set V%d = $0x%.2x\n", (operation & 0x0F00) >> 8, operation & 0x00FF);
    chip->cpu.v[(operation & 0x0F00) >> 8] = (operation & 0x00FF);
}

void execute_nibble_7(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(7) - Set V%d = V%d + $0x%.2x\n", (operation & 0x0F00) >> 8, (operation & 0x0F00) >> 8, operation & 0x00FF);
    chip->cpu.v[(operation & 0x0F00) >> 8] += (operation & 0x00FF);
}

void execute_nibble_8(struct Chip8 *chip, unsigned short int operation){
    unsigned int x = (operation & 0x0F00) >> 8;
    unsigned int y = (operation & 0x00F0) >> 4;
    int tmp;

    sprintf(chip->cpu.debug, "(8) - ");
    switch(operation & 0x000F){
        case 0x0: 
            sprintf(chip->cpu.debug, "Set V%d = V%d\n", x, y);
            chip->cpu.v[x] = chip->cpu.v[y];
            break;
        case 0x1: 
            sprintf(chip->cpu.debug, "Set V%d = V%d OR V%d\n", x, x, y);
            chip->cpu.v[x] |= chip->cpu.v[y]; 
            break;
        case 0x2: 
            sprintf(chip->cpu.debug, "Set V%d = V%d AND V%d\n", x, x, y);
            chip->cpu.v[x] &= chip->cpu.v[y]; 
            break;
        case 0x3:
            sprintf(chip->cpu.debug, "Set V%d = V%d XOR V%d\n", x, x, y);
            chip->cpu.v[x] ^= chip->cpu.v[y]; 
            break;
        case 0x4:
            sprintf(chip->cpu.debug, "Set V%d = V%d + V%d [set Carry]\n", x, x, y);
            tmp = (int)chip->cpu.v[x] + (int)chip->cpu.v[y] ;
            chip->cpu.v[0xF] = tmp > 0xFF;
            chip->cpu.v[x] = (char)(tmp & 0xFF);
            break;
        case 0x5:
            sprintf(chip->cpu.debug, "Set V%d = V%d - V%d\n\n", x, x, y);
            chip->cpu.v[0xF] = (chip->cpu.v[x] > chip->cpu.v[y]);
            chip->cpu.v[x] -= chip->cpu.v[y];
            break;
        case 0x6:
            sprintf(chip->cpu.debug, "Set V%d = V%d SHR V%d\n", x, x, y);
            chip->cpu.v[0xF] = (chip->cpu.v[x] & 0x01);
            chip->cpu.v[x] >>= 1;
            break;
        case 0x7:
            sprintf(chip->cpu.debug, "Set V%d = V%d - V%d (Set VF)\n", x, x, y);
            chip->cpu.v[0xF] = (chip->cpu.v[y] > chip->cpu.v[x]);
            chip->cpu.v[x] = chip->cpu.v[y] - chip->cpu.v[x];
            break;
        case 0xe:
            sprintf(chip->cpu.debug, "Set V%d = V%d SHL 1\n", x, x);
            chip->cpu.v[0xF] = (chip->cpu.v[x] & 0x80) >> 7;
            chip->cpu.v[x] <<= 1;
            break;
        default: 
            sprintf(chip->cpu.debug, "Error: unknown opcode %.4x\n", operation);
            break;
    }
}

void execute_nibble_9(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(9) - Skip if V%d != V%d\n", (operation & 0x0F00) >> 8, (operation & 0x00F0) >> 4);
    if(chip->cpu.v[(operation & 0x0F00) >> 8] != chip->cpu.v[(operation & 0x00F0) >> 4]){
        chip->cpu.pc += 2;
    }
}

void execute_nibble_a(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(a) - I = 0x%.4x\n", operation & 0x0FFF);
    chip->cpu.i = (operation & 0x0FFF);
}

void execute_nibble_b(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(b) - Jump to 0x%.4x + V0\n", operation & 0x0FFF);
    chip->cpu.pc = chip->cpu.v[0] + (operation & 0x0FFF);
}

void execute_nibble_c(struct Chip8 *chip, unsigned short int operation){
    sprintf(chip->cpu.debug, "(c) - Set V%d = (Random byte & 0x%.2x)\n", (operation & 0x0F00) >> 8, operation & 0x00FF);
    chip->cpu.v[(operation & 0x0F00) >> 8] = (rand() % (MAX_RAND + 1)) & (operation & 0x00FF);
}

void execute_nibble_d(struct Chip8 *chip, unsigned short int operation){
     unsigned int i, j;
     unsigned char x = chip->cpu.v[(operation & 0x0F00) >> 8];
     unsigned char y = chip->cpu.v[(operation & 0x00F0) >> 4];
     unsigned char n = (operation & 0x000F);
     chip->cpu.v[0xF] = 0;
     sprintf(chip->cpu.debug, "(d) Display\n");

     for(i = 0; i < n; i++){
         unsigned char byte = chip->mem.memory[chip->cpu.i + i];
         for(j = 0; j < 8; j++){
             if(byte & (0x80 >> j)){
                 if(chip->graphics[x + j + ((y + i) * DISPLAY_WIDTH)]){
                    chip->cpu.v[0xF] = 0x1;
                 }
                chip->graphics[x + j + ((y + i) * DISPLAY_WIDTH)] ^= 0x1; 
             }
         }
     }
}

void execute_nibble_e(struct Chip8 *chip, unsigned short int operation){
    unsigned char low = (operation & 0x00FF);

    switch(low){
        case 0x9e:
            sprintf(chip->cpu.debug, "(e) - Skip if V%d is pressed\n", (operation & 0x0F00) >> 8);
            if(chip->input[chip->cpu.v[(operation & 0x0F00) >> 8]]){
                chip->cpu.pc += 2;
            }
            break;
        case 0xa1:
            sprintf(chip->cpu.debug, "(e) - Skip if V%d is not pressed\n", (operation & 0x0F00) >> 8);
            if(!chip->input[chip->cpu.v[(operation & 0x0F00) >> 8]]){
                chip->cpu.pc += 2;
            }
            break;
        default: 
            sprintf(chip->cpu.debug, "Error: (e) unknown opcode %.4x\n", operation);
            break;
    }
}

void execute_nibble_f(struct Chip8 *chip, unsigned short int operation){
    unsigned int i;
    unsigned char low = (operation & 0x00FF);
    unsigned char x = (operation & 0x0F00) >> 8;
    int key;
    SDL_Event event;

    switch(low){
        case 0x07:
            sprintf(chip->cpu.debug, "(f) - V%d = DT\n", x);
            chip->cpu.v[x] = chip->cpu.delay;
            break;
        case 0x0a:
            sprintf(chip->cpu.debug, "(f) - Waiting for key press...\n");
            key = -1;
            while(key == -1){
                if(SDL_WaitEvent(&event)){
                    if(event.type == SDL_KEYDOWN){
                        for(i = 0; i < NUMBER_OF_KEYS; i++){
                            if(event.key.keysym.scancode == keys[i]){
                                key = i;
                                chip->cpu.v[x] = i;
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case 0x15:
            sprintf(chip->cpu.debug, "(f) - Set DT = V%d\n", x);
            chip->cpu.delay = chip->cpu.v[x];
            break;
        case 0x18:
            sprintf(chip->cpu.debug, "(f) - Set ST = V%d\n", x);
            chip->cpu.sound = chip->cpu.v[x];
            break;
        case 0x1e:
            sprintf(chip->cpu.debug, "(f) - I = I + V%d\n", x);
            chip->cpu.i += chip->cpu.v[x];
            break;
        case 0x29:
            sprintf(chip->cpu.debug, "(f) - Set I = addr_of_char('%d')\n", chip->cpu.v[x]);
            chip->cpu.i = chip->cpu.v[x] * 5; 
            /* Fonts are stored starting at 0x0.
             * A sprite for a character is 5 pixels high.
             * A row is stored in one byte so every character is 
             * on 5 bytes hence the '* 5'.*/
            break;
        case 0x33:
            sprintf(chip->cpu.debug, "(f) - Storing BDC of V%d(%d)\n", x, chip->cpu.v[x]);
            chip->mem.memory[chip->cpu.i] = chip->cpu.v[x] / 100;
            chip->mem.memory[chip->cpu.i + 1] = (chip->cpu.v[x] / 10) % 10;
            chip->mem.memory[chip->cpu.i + 2] = (chip->cpu.v[x] % 100) % 10;
            break;
        case 0x55:
            sprintf(chip->cpu.debug, "(f) - Writing registers from V0 to V%d starting at 0x%.4x\n", x, chip->cpu.i);
            for(i = 0; i <= x; i++){
                chip->mem.memory[chip->cpu.i + i] = chip->cpu.v[x];
            }
            break;
        case 0x65:
            sprintf(chip->cpu.debug, "(f) - Loading registers from V0 to V%d starting at 0x%.4x\n", x, chip->cpu.i);
            for(i = 0; i <= x; i++){
                chip->cpu.v[i] = chip->mem.memory[chip->cpu.i + i];
            }
            break;
        default: 
            sprintf(chip->cpu.debug, "Error: (f) unknown opcode %.4x\n", operation);
            break;
    }
}


# Chip-8 emulator

Chip-8 emulator written in C.


# Build & Run
```
git clone https://framagit.org/quentin-debroise/chip8-emulator
cd chip8-emulator/
make
cd bin/
./a.out ../roms/TETRIS
```

# License
CC-BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

# References

- http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
- http://www.emulator101.com.s3-website-us-east-1.amazonaws.com/
- http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/

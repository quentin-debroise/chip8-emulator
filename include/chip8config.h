/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 24 Dec 2017 
 */

/**
 * @file include/chip8config.h
 */

#ifndef __CHIP8CONFIG_H
#define __CHIP8CONFIG_H

/* Chip8 general constants */
#define MAX_RAND 255    /* Max random number */
#define RATE 60         /* 60 opcodes/s */     
#define CYCLE_TIME_MS (int)((1.0 / RATE) * 1000)    /* Duration of one CPU cycle */

/* CPU constants */
#define NB_REGISTERS 16         /* Number of registers */
#define NB_NIBBLES 16           /* Number of opcodes */

/* Memory constants */
#define MEMORY_SIZE 4096        /* 4K Memory */
#define PROGRAM_ADDR 0x200      /* Program and data starts at address 0x200 */
#define STACK_SIZE 16           /* Stack of 16 values */
#define FONT_START_ADDR 0x50    /* Font starts at address 0x50 in memory */
#define NB_FONT_BYTES 16 * 5    /* Number of bytes in the font. A char is 8x5 pixels */

/* Graphics constants */
#define DISPLAY_WIDTH 64        /* 64 pixels wide */
#define DISPLAY_HEIGHT 32       /* 32 pixels high */

/* Input constants */
#define NUMBER_OF_KEYS 16       /* Number of keys on a chip8 */

extern const unsigned int keys[0x10];    /* Key layout. Defined in chip8.c */


#endif



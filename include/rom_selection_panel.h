/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 02 Jan 2018 
 */

/**
 * @file include/rom_selection_panel.h
 */

#ifndef __ROM_SELECTION_PANEL_H
#define __ROM_SELECTION_PANEL_H

#include <SDL2/SDL.h>
#include <render.h>
#include <panel.h>

struct _LinkedList;

typedef struct ROMButton{
    char *label;
    SDL_Rect position;
}*rombutton_t;

typedef struct ROMSelectionPanel{
    panel_t panel;
    struct _LinkedList *buttons;
    int needsUpdate;
    SDL_Rect selection;
    SDL_Rect hovering;
    SDL_Color textcolor;
}rompanel_t;

/**
 * Creates a new ROM selection button. Position is relative to
 * the rom selection panel.
 *
 * @param label Label on the button
 * @param position Position of the button relative to the rom selection panel.
 *
 * @return A rom button that needs to be freed with rombutton_free()
 */
extern rombutton_t rombutton_create(char *label, SDL_Rect position);

/**
 * Frees a previously created rom button.
 *
 * @param button Rom button to free.
 */
extern void rombutton_free(void *button);

/**
 * Initializes the ROM selection panel.
 *
 * @param panel Panel to init.
 */
extern rompanel_t* init_rom_selection_panel();

/**
 * Render the rom selection panel to the screen.
 *
 * @param graphics The graphic structure.
 * @param panel Panel to render.
 */
extern void rom_selection_panel_draw(const graphics_t *graphics, rompanel_t *rompanel);

extern int rompanel_inside_button(SDL_Rect butpos, const SDL_Event *event);

extern void rompanel_handle_mousemotion(rompanel_t *rompanel, const SDL_Event *event);

/**
 * Handle mouse down button in the rom panel area.
 *
 * @param rompanel Rom selection panel.
 * @param event Event structure.
 *
 * @return the rom name as it is in the rom folder if user clicked on a ROM. NULL otherwise.
 */
extern char* rompanel_handle_buttondown(rompanel_t *rompanel, const SDL_Event *event);

#endif



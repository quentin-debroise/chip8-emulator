/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 */

/**
 * @file include/emulatorconfig.h
 */

#ifndef __EMULATORCONFIG_H
#define __EMULATORCONFIG_H

/*#define SQUARE_SIZE 10  [> Size a of a square in game in pixels <]*/
#define WIN_WIDTH DISPLAY_WIDTH * SQUARE_SIZE   /* Width of the emulator window */
#define WIN_HEIGHT DISPLAY_HEIGHT * SQUARE_SIZE /* Height of the emulator window */

/*#define ROM_FOLDER "../roms/"    [> Name of the ROMs directory <]*/

extern char *BASEPATH;  /* Basepath */
extern char *ROMPATH;  /* Path to ROMs directory */

extern int SQUARE_SIZE;
extern char *ROM_FOLDER;
extern char *FONT;

extern long HOVERING_COLOR;
extern long TEXT_COLOR;

extern int FONT_SIZE;



extern void config_load(char *basepath);

#endif



/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 19 Dec 2017 
 */

/**
 * @file include/chip8_rom.h
 */

#ifndef __CHIP8_ROM_H
#define __CHIP8_ROM_H

struct Chip8Mem;

/**
 * Loads into memory a CHIP-8 program.
 *
 * @param mem memory_t object.
 * @param filepath Path to CHIP-8 program.
 *
 * @return The number of bytes loaded. 0 if loading failed.
 */
extern unsigned int rom_load(struct Chip8Mem *mem, const char *filepath);

/**
 * Reads a CHIP-8 ROM.
 *
 * @param filepath Path to the ROM file.
 * @param buffer [OUT] Buffer containing the code.Set to NULL if an error occurs.
 * @info The buffer needs to be freed to avoid memory leaks.
 *
 * @return Number of bytes read. 0 if an error occured
 */
extern unsigned int rom_read(const char *filepath, unsigned char **buffer);

/**
 * Outputs the content of a CHIP-8 program in hexadecimal to the terminal.
 *
 * @param filepath Path to the CHIP-8 program.
 */
extern void rom_hexdump(const char *filepath);

/**
 * Dissassemble a CHIP-8 ROM. Outputs the equivalent assembly to the standard output.
 *
 * @param codebuffer Buffer containing the code in the ROM.
 * @param codeLength Length of the code.
 */
extern void rom_dissassemble(const unsigned char *codebuffer, unsigned int codeLength);

#endif



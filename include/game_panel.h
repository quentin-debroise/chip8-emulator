/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 */

/**
 * @file include/game_panel.h
 */

#ifndef __GAME_PANEL_H
#define __GAME_PANEL_H

#include <SDL2/SDL.h>
#include <emulatorconfig.h>
#include <chip8_graphics.h>
#include <panel.h>


extern panel_t* init_game_panel();
extern void game_panel_draw(chip8_graphics_t gfx,
        const panel_t *panel,
        SDL_Renderer *renderer);

#endif



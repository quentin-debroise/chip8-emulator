/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 20 Dec 2017 
 */

/**
 * @file include/render.h
 */

#ifndef __RENDER_H
#define __RENDER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <emulatorconfig.h>
#include <chip8_graphics.h>
#include <chip8_input.h>
#include <panel.h>

struct Emulator;
struct ROMSelectionPanel;

typedef struct Graphics{
    SDL_Window *window;
    SDL_Renderer *renderer;
    TTF_Font *font;
    struct Panel *panels[3];
    struct ROMSelectionPanel *rompanel;
}graphics_t;

/**
 * Initializes the SDL and all needed modules
 */
extern void render_init();

/**
 * Create and initialize a new SDL window with a renderer.
 *
 * @param graphics Graphic structure.
 */
extern void render_init_window(struct Graphics *graphics);

/**
 * Destroy the window and renderer, then quit SDL.
 *
 * @param graphics Graphic structure.
 */
extern void render_destroy(struct Graphics *graphics);

extern void render_draw(chip8_graphics_t gfx, const struct Graphics *graphics);

extern void render_handle_event(struct Emulator *emulator, const SDL_Event *event);

extern SDL_Texture* render_create_text(const struct Graphics *font, const char *text, SDL_Color color, int *w, int *h);

#endif



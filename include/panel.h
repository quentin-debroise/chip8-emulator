/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 26 Dec 2017 
 */

/**
 * @file include/panel.h
 */

#ifndef __PANEL_H
#define __PANEL_H

#include <SDL2/SDL.h>

typedef struct Panel{
    SDL_Rect viewport;
    SDL_Rect relative;
    SDL_Color bgcolor;
}panel_t;

extern void panel_fill_background(SDL_Renderer *renderer, const panel_t *panel);

extern void panel_init_relative(panel_t *panel);

extern void panel_background_color(panel_t *panel,
        unsigned int r,
        unsigned int g,
        unsigned int b,
        unsigned int a);

#endif



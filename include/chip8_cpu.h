/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 10 Dec 2017 
 */

/**
 * @file include/chip8_cpu.h
 *
 * CHIP-8 CPU functions for executing each instruction.
 */

#ifndef __CHIP8_CPU_H
#define __CHIP8_CPU_H

/* Forward declared. Delcared in chip8.h */
struct Chip8;

typedef void(*ptr_execute_nibble_t)(struct Chip8*, unsigned short int);

typedef enum{
    CPU_STOP,
    CPU_RESET,
    CPU_RUNNING,
    CPU_DEBUG
}cpu_state_t;

/*
 * Representation of the CHIP-8 CPU. Holds all the CHIP-8 registers.
 */
typedef struct Chip8CPU{
    unsigned char v[16];    /* 16 8-bits registers from V0 to VF */
    unsigned short int i;   /* 16-bits register, stores memory addresses */
    unsigned short int pc;  /* Program counter (Instruction pointer) */
    unsigned char sp;       /* Stack pointer */
    unsigned char delay;    /* 8-bits register for the delay timer */
    unsigned char sound;    /* 8-bits register for the sound timer */

    cpu_state_t state;      /* CPU state */
    char debug[256];        /* Holds information about the current instruction. */
}chip8_cpu_t;

/**
 * Resets the chip8 cpu to its default values
 *
 * @param cpu Chip8 CPU structure.
 */
extern void cpu_reset(struct Chip8CPU *cpu);

/**
 * Executes one cpu cycle.
 *
 * @param chip Chip8 structure.
 * @param timerDecrease Boolean value set to 1 when ther sound and delay timers need to be decreased. Then sets it to 0. If 0 is passed then the timers aren't decreased.
 */
extern void cpu_execute_cycle(struct Chip8 *chip, int *timerDecrease);


/*
 * Each function executes one category of instruction.
 */
extern void execute_nibble_0(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_1(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_2(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_3(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_4(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_5(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_6(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_7(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_8(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_9(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_a(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_b(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_c(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_d(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_e(struct Chip8 *chip, unsigned short int operation);
extern void execute_nibble_f(struct Chip8 *chip, unsigned short int operation);

#endif



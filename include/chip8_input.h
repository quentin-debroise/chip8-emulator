/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 20 Dec 2017 
 */

/**
 * @file include/chip8_input.h
 */

#ifndef __CHIP8_INPUT_H
#define __CHIP8_INPUT_H

#include <chip8config.h>

typedef unsigned char chip8_input_t[NUMBER_OF_KEYS];

/**
 * Reset the input array so that no key is pressed by default.
 *
 * @param input Chip8 input structure.
 */
extern void input_reset(chip8_input_t input);

/**
 * Checks if the given key is pressed.
 *
 * @param input Chip8 input structure.
 * @param key The key to check if it is being pressed.
 *
 * @return 1 if the key is pressed, 0 otherwise.
 */
extern int input_is_key_pressed(chip8_input_t input, unsigned char key);

#endif



/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 18 Dec 2017 
 */

/**
 * @file include/chip8_mem.h
 */

#ifndef __CHIP8_MEM_H
#define __CHIP8_MEM_H

#include <chip8config.h>

/**
 * Holds the CHIP-8 memories
 *
 * == Memory map ==
 *
 * 0x000 - 0x1FF    Reserved for interpreter
 * 0x200 - 0xFFF    Program ROM and data
 */
typedef struct Chip8Mem{
    unsigned char memory[MEMORY_SIZE];      /* memory space 4K (4096 bytes) */
    unsigned short int stack[STACK_SIZE];   /* Stack of 16 16-bits values */
}chip8_mem_t;

/**
 * Initializes the chip8 memory.
 *
 * @param mem Chip8 memory structure.
 */
extern void chip8_mem_init(chip8_mem_t *mem);

#endif



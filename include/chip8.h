/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 09 Dec 2017 
 */

/**
 * @file include/chip8.h
 */

#ifndef __CHIP8_H
#define __CHIP8_H

#include <chip8config.h>
#include <chip8_rom.h>
#include <chip8_cpu.h>
#include <chip8_mem.h>
#include <chip8_input.h>
#include <chip8_graphics.h>


typedef struct Chip8{
    chip8_mem_t mem;
    chip8_cpu_t cpu;
    chip8_graphics_t graphics;
    chip8_input_t input;
}chip8_t;


/**
 * Dumps the state of registers and stack to the standard output.
 *
 * @param chip Chip8 structure.
 */
extern void chip8_dump(chip8_t chip);

/**
 * Resets the chip. All the registers, the stack and the display.
 *
 * @param chip The chip8 structure.
 */
extern void chip8_reset(chip8_t *chip);

/**
 * Execute the loaded program step by step.
 *
 * @param chip The chip8 structure.
 */
extern void chip8_execute_step_by_step(chip8_t *chip);

/**
 * Executes the given number of instructions of the program then stops.
 *
 * @param chip The chip8 structure.
 * @param n Number of instructions to execute.
 */
extern void chip8_execute_n(chip8_t *chip, unsigned int n);

/**
 * Executes the loaded program.
 *
 * @param chip The chip8 structure.
 */
extern void chip8_execute(chip8_t *chip);

#endif



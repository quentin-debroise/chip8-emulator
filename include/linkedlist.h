/*
 * Quentin Debroise
 * 7 route du clos hubert
 * 50300 Marcey les greves
 *
 * COPYRIGHT
 */

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.1 - 19 Mar 2017 
 */

/**
 * @file linkedlist.h
 *
 * Linked list.
 */

#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H

typedef struct _LLCell{
    void *data;
    struct _LLCell *next;
}llcell_t;

/**
 * @struct linkedlist_t
 * @brief Linked list representation
 *
 * @var linkedlist_t::head
 * A pointer on the first cell of the linked list.
 * @var linkedlist_t::tail
 * A pointer on the last cell of the linked list.
 * @var linkedlist_t::current
 * A pointer on the current cell (use as an iterator)
 * @var linkedlist_t::equals
 * A function pointer that must be definied in order to compare an element of
 * the linked list with an other element. The function must return an integer
 * greater then 1 if the elements are the same, 0 otherwise.
 * @var linkedlist_t::free_data
 * A function pointer that must be definied if data holds complex types that
 * cannot be freed by a simple call to the standard free function.
 * If it is definied then this function is used instead of the usual
 * free function.
 */
typedef struct _LinkedList{
    llcell_t *head;
    llcell_t *tail;
    llcell_t *current;
    int (*equals)(void *a, void *b);
    void (*free_data)(void *data);
}*linkedlist_t;


/**
 * Creates a new linked list.
 *
 * @return The newly created linked list.
 */
linkedlist_t ll_create();

/**
 * Tests if a linked list is empty.
 *
 * @param ll The linked list.
 *
 * @return 1 is the linked list is empty, 0 otherwise.
 */
int ll_is_empty(linkedlist_t ll);

/**
 * Destroys a linked list.
 *
 * @param ll A pointer on the linked list to destroy. The linked list is
 * set to NULL after all the elements have been destroyed.
 */
void ll_destroy(linkedlist_t ll);

/**
 * Adds an element to the end of a linked list.
 *
 * @param ll The linked list.
 * @param data The data to add to the linked list.
 *
 * @return 0 if everything went fine, 1 if an error occured during the insertion.
 * If 1 is returned then the element isn't added to the linked list.
 */
int ll_add(linkedlist_t ll, void *data);

/**
 * Clears a linked list from all its elements
 *
 * @param ll The linked list to clear.
 */
void ll_clear(linkedlist_t ll);

/**
 * Removes the first element of the linked list than is equal to the data
 * given as a parameter.
 *
 * @param ll The linked list.
 * @param data The data that must be removed from the linked list.
 *
 * @return 1 if the element has been found and removed, 0 otherwise.
 * If 0 is returned then the element has not been removed.
 */
int ll_remove(linkedlist_t ll, void *data);

/**
 * Checks if the iterator has reached the end of the list.
 *
 * @param ll The linked list.
 *
 * @return 1 if the linked list has more elements, 0 otherwise.
 */
int ll_has_next(linkedlist_t ll);

/**
 * Returns the current element pointed by the iterator.
 *
 * @param ll The linked list.
 *
 * @return The current element, NULL otherwise.
 */
void* ll_next(linkedlist_t ll);

/**
 * Checks wether or not the linked list contains a given element.
 *
 * @param ll The linked list.
 * @param element The element to search for in the linked list.
 *
 * @return 1 if the element has been found in the list, 0 otherwise.
 */
int ll_contains(linkedlist_t ll, void *element);

/**
 * Get the current element.
 *
 * @param ll The linked list.
 *
 * @return The data of the current element.
 */
void* ll_get_current(linkedlist_t ll);
#endif



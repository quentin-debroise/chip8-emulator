/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 24 Dec 2017 
 */

/**
 * @file include/emulator.h
 */

#ifndef __EMULATOR_H
#define __EMULATOR_H

#include <chip8.h>
#include <render.h>

typedef struct Emulator{
    chip8_t chip;
    graphics_t graphics;
    int timerDecrease;
    char *loadedROM;
}emulator_t;


/**
 * Resets the emulator. i.e. all components. Doesn't init graphics again.
 *
 * @param emulator The emulator.
 */
extern void emulator_reset(emulator_t *emulator);

/**
 * Initializes the emulator.
 *
 * @param emulator Emulator.
 */
extern void emulator_init(emulator_t *emulator);

/**
 * Load ROM emulator.
 *
 * @param emulator Emulator.
 * @param rompath Path to the ROM.
 *
 * @return -1 if the loading failed else the number of bytes loaded (number > 0)
 */
extern int emulator_load_rom(emulator_t *emulator, char *rom);

/**
 * Runs the emulator.
 *
 * @param emulator Emulator.
 */
extern void emulator_run(emulator_t *emulator);

extern void emulator_update(emulator_t *emulator);

/**
 * Quit the emulator.
 *
 * @param emulator Emulator.
 */
extern void emulator_quit(emulator_t *emulator);

#endif



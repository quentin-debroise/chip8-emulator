/*
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/**
 * @author Quentin Debroise <debroise.quentin@gmail.com>
 * @version     0.0.0 - 18 Dec 2017 
 */

/**
 * @file include/chip8_graphics.h
 */

#ifndef __CHIP8_GRAPHICS_H
#define __CHIP8_GRAPHICS_H

#include <chip8config.h>

typedef unsigned char chip8_graphics_t[DISPLAY_HEIGHT * DISPLAY_WIDTH];


/*
 * Clears the display.
 *
 * @param graphics chip8 graphics.
 */
extern void display_clear(chip8_graphics_t graphics);

/*
 * Prints the graphics to the standard output.
 *
 * @param graphics The chip8 graphics.
 */
extern void display(chip8_graphics_t graphics);

#endif



ROOT_DIR = $(shell pwd)
BIN_DIR = $(ROOT_DIR)/bin
O = $(ROOT_DIR)/obj
LIB_DIR = $(ROOT_DIR)/lib
SRC_DIR = $(ROOT_DIR)/src
INCLUDE_DIR = $(ROOT_DIR)/include
export ROOT_DIR BIN_DIR O LIB_DIR SRC_DIR INCLUDE_DIR

CC = gcc 
RM = rm -f
CPPFLAGS = -Wall -Wextra -ansi -pedantic -I$(INCLUDE_DIR)
CFLAGS = -g -c
LDFLAGS = -Wl,-rpath $(LIB_DIR) -L$(LIB_DIR) `sdl2-config --cflags --libs`
LIBS = -llinkedlist -lSDL2 -lSDL2_ttf

EXE = $(BIN_DIR)/a.out

INCLUDES = $(wildcard $(INCLUDE_DIR)/*.h)
OBJS = \
	   $(O)/panel.o	\
	   $(O)/game_panel.o	\
	   $(O)/disassembly_panel.o	\
	   $(O)/rom_selection_panel.o	\
	   $(O)/emulator.o	\
	   $(O)/emulatorconfig.o	\
	   $(O)/render.o	\
	   $(O)/chip8_graphics.o	\
	   $(O)/chip8_input.o	\
	   $(O)/chip8_mem.o	\
	   $(O)/chip8_cpu.o	\
	   $(O)/chip8_rom.o	\
	   $(O)/chip8.o	\
	   $(O)/main.o


all: $(EXE)

$(EXE): $(OBJS) $(INCLUDES)
	@mkdir -p $(BIN_DIR)
	$(CC) $(OBJS) -o $(EXE) $(LDFLAGS) $(LIBS)

$(O)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(O)
	$(CC) $(CPPFLAGS) $(CFLAGS) $< -o $@



clean:
	@$(RM) $(O)/*.o

dist-clean:
	@make clean
	@$(RM) $(BIN_DIR)/*


.PHONY: all build clean dist-clean build

